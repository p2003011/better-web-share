/* eslint-disable unicorn/no-array-reduce */
/* eslint-disable unicorn/no-unreadable-iife */
/* eslint-disable no-unused-vars */

/*
    LIFWEB 2024 -- CC2 TP notÃ©
    SUJET B - 11:30

    ComplÃ©ter les exercices demandÃ©s en 60 minutes (hors tiers-temps).
    DÃ©poser dans la cellule correspondante de Tomuss le fichier JS complÃ©tÃ© et UNIQUEMENT ce fichier.

    Les rendus par mail ne seront PAS pris en compte, sauf si vous Ãªtes tiers-temps, auquel cas vous devez envoyer votre fichier Ã  romuald.thion@univ-lyon1.fr.
    Un fichier qui ne se charge pas correctement ne sera PAS pris en compte.

    Tous les documents et l'accÃ¨s Ã  internet sont autorisÃ©s : votre code, les corrections, MDN, stack-overflow, etc.
    Vous pouvez utiliser votre ordinateur personnel ou les ordinateurs de la salle de TP.

    Toute communication entre humains est INTERDITE.
    Les IAs/LLMs (ChatGPT, GitHub Copilot, etc.) sont INTERDITS.
    Si vous utilisez Copilot ou un outil similaire dans votre IDE, DÃ‰SACTIVEZ-LE. 
*/

// On donne la "base de donnÃ©es" suivante

const database = [
  { name: "steel", url: "https://pokeapi.co/api/v2/type/9/" },
  { name: "grass", url: "https://pokeapi.co/api/v2/type/12/" },
  { name: "fighting", url: "https://pokeapi.co/api/v2/type/2/" },
  { name: "water", url: "https://pokeapi.co/api/v2/type/11/" },
  { name: "fire", url: "https://pokeapi.co/api/v2/type/10/" },
  { name: "flying", url: "https://pokeapi.co/api/v2/type/3/" },
  { name: "ground", url: "https://pokeapi.co/api/v2/type/5/" },
  { name: "rock", url: "https://pokeapi.co/api/v2/type/6/" },
  { name: "ghost", url: "https://pokeapi.co/api/v2/type/8/" },
  { name: "bug", url: "https://pokeapi.co/api/v2/type/7/" },
  { name: "electric", url: "https://pokeapi.co/api/v2/type/13/" },
  { name: "normal", url: "https://pokeapi.co/api/v2/type/1/" },
  { name: "poison", url: "https://pokeapi.co/api/v2/type/4/" },
];

// Exercice 1
//
// Ecrire un dÃ©corateur de fonction qui prend une fonction f unaire et n'exÃ©cute
// f que si l'argument est de type number et qu'il est (strictement) positif.
// Sinon, on renvoie undefined sans exÃ©cuter f.
//
// Donner de quoi vÃ©rifier le nombre d'appels au paramÃ¨tre f avec un exemple

function positiveNumber(f) {
  return function wrapped(argument) {
    if (typeof argument == "number" && argument > 0)
      return f((console.log("I'm being called"), argument));
    return;
  };
}

function test(number) {
  return number;
}

// Tests:
console.log(positiveNumber(test)(4));
console.log(positiveNumber(test)(-1)); // renvois undefined et il n y a pas d'appel à f
console.log(positiveNumber(test)(0)); // renvois undefined et il n y a pas d'appel à f

// Exercice 2
//
// ComplÃ©ter la fonction suivante pour qu'elle gÃ©nÃ¨re des Ã©lements <div>
// chacune avec un Ã©lÃ©ment <a> ayant comme texte le nom de la catÃ©gorie
// Comme href, on utilisera le nom ${NAME} pour produire une URL de la forme suivante
// https://bulbapedia.bulbagarden.net/wiki/${NAME}_(type)
//
// La fonction doit retourner un DocumentFragment qui sera ajoutÃ© au <div> dans le DOM dans l'exercice 4.

function capitalize(word) {
  return word && word[0].toUpperCase() + word.slice(1);
}

function generateDivs(data) {
  const fragment = document.createDocumentFragment();

  for (const row of data) {
    const div = document.createElement("div");
    const a = document.createElement("a");

    const NAME = capitalize(row.name); // j'aurais nommée la variable name mais je suis les indications plus haut
    a.href = `https://bulbapedia.bulbagarden.net/wiki/${NAME}_(type)`;
    a.textContent = NAME;

    div.append(a);
    fragment.append(div);
  }

  return fragment;
}

// pour tester
const testDiv = document.createElement("div");
testDiv.append(generateDivs(database));
console.log(testDiv.innerHTML); // devra être retiré / deprecated
// doit afficher quelque chose comme suit, modulo indentation

// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Steel_(type)">Steel</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Grass_(type)">Grass</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Fighting_(type)">Fighting</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Water_(type)">Water</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Fire_(type)">Fire</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Flying_(type)">Flying</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Ground_(type)">Ground</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Rock_(type)">Rock</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Ghost_(type)">Ghost</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Bug_(type)">Bug</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Electric_(type)">Electric</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Normal_(type)">Normal</a></div>
// <div><a href="https://bulbapedia.bulbagarden.net/wiki/Poison_(type)">Poison</a></div>

// Exercice 3
//
// Le contenu de la base de donnÃ©es est disponible dans le fichier suivant :
// http://lifweb.pages.univ-lyon1.fr/CC/CC2-b-WoongeefeFeet5ch/lifweb-2024-cc2-sujet-b.json
//
// Une base de page web est disponible dans le fichier suivant :
// http://lifweb.pages.univ-lyon1.fr/CC/CC2-b-WoongeefeFeet5ch/lifweb-2024-cc2-sujet-b.html
//
// Ajouter la balise <script> au <head> de la page pour inclure le JS
// dans le fichier HTML.
//
// ->			<script defer src="rendu-theo-labrosse.js"></script>
//
// Ajouter ci-aprÃ¨s le code nÃ©cessaire pour qu'aussitÃ´t que le DOM est chargÃ©,
//
// 1. On tÃ©lÃ©charge le fichier JSON.
// 2. On le parse en tant que JSON.
// 3. On remplit <div id="pokemons"></div> avec les ancres du fichier JSON.
// 4. On ajoute un Ã©couteur d'Ã©vÃ©nement sur chaque lien pour afficher le lien
//    dans la console quand on passe la souris dessus (Ã©vÃ©nement "mouseover")
//
// Utiliser un style async/await
//
// Pour la moitiÃ© des points, utiliser la constante `database` pour une version statique.
// Vous pourrez avoir besoin d'une IIFE async.

const uri =
  "http://lifweb.pages.univ-lyon1.fr/CC/CC2-b-WoongeefeFeet5ch/lifweb-2024-cc2-sujet-b.json";

// Exo 3
(() => {
  // Fetch le fichier et le parse en JSON
  async function fetchJson(uri) {
    const jsonResponse = await fetch(uri, {
      method: "GET",
    }).then((response) => response.json());

    return jsonResponse;
  }

  // Load le JSON et le place dans le div
  async function loadJson(uri) {
    const jsonResponse = await fetchJson(uri);
    //console.log(jsonResponse);

    const fragment = generateDivs(jsonResponse);
    //console.log(fragment);

    const $pokemonDiv = document.querySelector("#pokemons");
    $pokemonDiv.append(fragment);

    // Met en place l'event listener
    for (const div of $pokemonDiv.children) {
      div.firstChild.addEventListener("mouseover", (event) => {
        console.log(event.target.href);
      });
    }
  }

  loadJson(uri);
})();

// Exercice 4
//
// - Donner des noms significatifs aux variables
// - Expliquer le code en commentaires (idÃ©alement, JSDoc)
// - Donner un exemple d'utilisation de la fonction

/**
 * la fonction fait la somme d'un array, elle retourne un objet qui contient d'une part la somme
 * et de l'autre un compteur qui ocmpte le nombre d'itération (taille de l'array initial)
 *
 * si l'array est vide, retourne l'objet {somme: 0, compteur: 0}
 *
 * @param {array} array
 * @returns {{somme: int, compteur: int}} un objet {somme: int, compteur: int}
 */
const ex4b = (array) =>
  array.reduce(
    ({ somme, compteur }, currentValue) => ({
      somme: somme + currentValue,
      compteur: compteur + 1,
    }),
    { somme: 0, compteur: 0 }, // initial value
  );

const list1 = [1, 2, 3, 4, 5];
console.log(ex4b(list1));

const list2 = [];
console.log(ex4b(list2));
