console.log("hello world"); 

(() => {
    async function GET(link) {
        console.log("GET" + link);
        const response = await fetch(link, 
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "X-API-KEY": "279af028-2d7d-4add-a5af-38abfba0f418",
                }
            }
        ).then((response) => response.json());

        // console.log(response);

        return response;
    }

    async function Testor() {
        const userResults = await GET('https://lifweb.univ-lyon1.fr/users');

        console.log("score:"); 
        console.log(userResults);
    }

    Testor();
    

})();

console.log("hello world"); 


(() => {
    async function GET(link) {
        console.log("GET" + link);
        const response = await fetch(link, 
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "X-API-KEY": "279af028-2d7d-4add-a5af-38abfba0f418",
                }
            }
        ).then((response) => response.json());

        // console.log(response);

        return response.href;
    }

    async function POST(link) {
        // console.log("POST" + link);
        const response = await fetch(link, 
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-API-KEY": "279af028-2d7d-4add-a5af-38abfba0f418",
                }
            }
        ).then((response) => response.json());

        // console.log(response);

        return { href : response.href, challenge : response.challenges[0]};
    }

    async function Solve(link, flag) {
        // console.log("Solve" + link + " " + flag);
        // console.log(flag)
        // console.log("{\n \"challenge\": \"flag\"}");

        const response = await fetch(link, 
            {
                method: "POST",
                headers: {
                    "accept": "application/json",
                    "Content-Type": "application/json",
                    "X-API-KEY": "279af028-2d7d-4add-a5af-38abfba0f418"
                    
                },
                body: `{\n "challenge": "${flag}" \n}`

            }
        ).then((response) => response.json());

    console.log(response);

    }

    async function Testor() {
        const linkStart = await GET('https://lifweb.univ-lyon1.fr/level/2');

        // console.log(linkStart);
        const solveObject =await POST(linkStart);

        // console.log(solveObject);

        Solve(solveObject.href, solveObject.challenge)
    }

    Testor();
    

})();