/* eslint-disable unicorn/prefer-top-level-await */
/* eslint-disable unicorn/no-unreadable-iife */
/* eslint-disable no-unused-vars */

/*
    LIFWEB 2024 -- CC2 TP notÃ©
    SUJET A - 09:45

    ComplÃ©ter les exercices demandÃ©s en 60 minutes (hors tiers-temps).
    DÃ©poser dans la cellule correspondante de Tomuss le fichier JS complÃ©tÃ© et UNIQUEMENT ce fichier.

    Les rendus par mail ne seront PAS pris en compte, sauf si vous Ãªtes tiers-temps, auquel cas vous devez envoyer votre fichier Ã  romuald.thion@univ-lyon1.fr.
    Un fichier qui ne se charge pas correctement ne sera PAS pris en compte.

    Tous les documents et l'accÃ¨s Ã  internet sont autorisÃ©s : votre code, les corrections, MDN, stack-overflow, etc.
    Vous pouvez utiliser votre ordinateur personnel ou les ordinateurs de la salle de TP.

    Toute communication entre humains est INTERDITE.
    Les IAs/LLMs (ChatGPT, GitHub Copilot, etc.) sont INTERDITS.
    Si vous utilisez Copilot ou un outil similaire dans votre IDE, DÃ‰SACTIVEZ-LE.
*/

// On donne la "base de donnÃ©es" suivante

const database = [
  { url: "https://cdn2.thecatapi.com/images/51t.jpg", width: 1500, height: 1500 },
  { url: "https://cdn2.thecatapi.com/images/ba4.jpg", width: 500, height: 333 },
  { url: "https://cdn2.thecatapi.com/images/c6c.jpg", width: 500, height: 328 },
  { url: "https://cdn2.thecatapi.com/images/d7u.jpg", width: 632, height: 960 },
  { url: "https://cdn2.thecatapi.com/images/dsa.jpg", width: 640, height: 418 },
  { url: "https://cdn2.thecatapi.com/images/MTUwMDU4MA.jpg", width: 500, height: 333 },
  { url: "https://cdn2.thecatapi.com/images/wLFWzKgkf.jpg", width: 819, height: 1024 },
];

// Exercice 1
//
// ComplÃ©ter la fonction suivante qui renvoie les urls en majuscule
// des images dont la surface est supÃ©rieure Ã  500_000 pixels.
//
// Pour avoir la totalitÃ© des points, l'Ã©crire de la faÃ§on la plus
// concise et fonctionnelle possible.
//
// La moitiÃ© des points est attribuÃ©e pour une solution correcte mais
// qui utilise des variables mutables ou des boucles.

const imagesLargerThan500k = (data) => {
  let res = [];
  for (element of data) {
    if (element.width * element.height > 500000) {
      res.push(element.url.toUpperCase());
    }
  }
  return res;
}; 

console.log(imagesLargerThan500k(database));

// on attend
// [
//   'HTTPS://CDN2.THECATAPI.COM/IMAGES/51T.JPG',
//   'HTTPS://CDN2.THECATAPI.COM/IMAGES/D7U.JPG',
//   'HTTPS://CDN2.THECATAPI.COM/IMAGES/WLFWZKGKF.JPG'
// ]

// Exercice 2
//
// ComplÃ©ter la fonction suivante pour qu'elle gÃ©nÃ¨re des Ã©lements <li>
// chacune avec un Ã©lÃ©ment <a> ayant comme texte et comme href l'url de l'image.
//
// La fonction doit retourner un DocumentFragment qui sera ajoutÃ© au <ul> dans le DOM dans l'exercice 4.

function generateLis(data) {

  res = document.createDocumentFragment();
  for (element of data) {
    const ElemLi = document.createElement("li");
    const ElemA = document.createElement("a");
    ElemA.href = element.url;
    ElemA.textContent = element.url;
    ElemLi.appendChild(ElemA);
    res.appendChild(ElemLi);
  }

  return res;
}

// pour tester
const testDiv = document.createElement("div");
testDiv.append(generateLis(database));
console.log(testDiv.innerHTML);
// doit afficher quelque chose comme suit, modulo indentation

// <ul id="cats">
// <li>
//   <a href="https://cdn2.thecatapi.com/images/51t.jpg"
//     >https://cdn2.thecatapi.com/images/51t.jpg</a
//   >
// </li>

//   ...

// <li>
//   <a href="https://cdn2.thecatapi.com/images/wLFWzKgkf.jpg"
//     >https://cdn2.thecatapi.com/images/wLFWzKgkf.jpg</a
//   >
// </li>
// </ul>

// Exercice 3
//
// Le contenu de la base de donnÃ©es est disponible dans le fichier suivant :
// http://lifweb.pages.univ-lyon1.fr/CC/CC2-a-xah7ahMigai8ohRe/lifweb-2024-cc2-sujet-a.json
//
// Une base de page web est disponible dans le fichier suivant :
// http://lifweb.pages.univ-lyon1.fr/CC/CC2-a-xah7ahMigai8ohRe/lifweb-2024-cc2-sujet-a.html
//
// Ajouter la balise <script> au <head> de la page pour inclure le JS
// dans le fichier HTML.
//
// Ajouter ci-aprÃ¨s le code nÃ©cessaire pour qu'aussitÃ´t que le DOM est chargÃ©,
//
// 1. On tÃ©lÃ©charge le fichier JSON.
// 2. On le parse en tant que JSON.
// 3. On remplit <ul id="cats"> avec les liens du fichier JSON.
// 4. On ajoute un Ã©couteur d'Ã©vÃ©nement sur chaque lien pour que quand on clique dessus,
//    l'image correspondante s'affiche dans <img id="selected-cat"> SANS redirection
//
// Utiliser un style async/await
//
// Pour la moitiÃ© des points, utiliser la constante `database` pour une version statique.
// Vous pourrez avoir besoin d'une IIFE async.

const uri =
  "http://lifweb.pages.univ-lyon1.fr/CC/CC2-a-xah7ahMigai8ohRe/lifweb-2024-cc2-sujet-a.json";
const ulCats = document.querySelector("#cats");


const img = document.querySelector("#selected-cat");

async function getDB(url) {
  try {
    const response = await fetch(url);
    db = await response.json();
    ulCats.append(generateLis(db));
    for (element of document.querySelectorAll("#cats > li > a")) {
        element.addEventListener("click", (event) => {
            event.preventDefault();
            const link = event.target.href;
            img.setAttribute("src", link);
        })

    }
  } catch (err) {
    console.log("fetch failed", err);
  }
}

getDB(uri);

// Exercice 4
//
// - Donner des noms significatifs aux variables
// - Expliquer le code en commentaires (idÃ©alement, JSDoc)
// - Donner un exemple d'utilisation de la fonction
// - Donner de quoi vÃ©rifier le nombre d'appels au paramÃ¨tre f avec un exemple

s
const ex4a = (y) => (x) => (z) => x <= 0 ? z : y(ex4a(y)(x - 1)(z));

const exo4 = (fonction) => (chiffre) => (retour) => chiffre <= 0 ? retour : fonction(exo4(fonction)(chiffre - 1)(retour));

/*
Si X inférieur ou égal a 0, renvoi z
sinon, appel a la fonction y, prenant en parametres (ex4a) appelée avec les memes parametres et x décrémenté

peut etre utilisé pour appeler la fonction y x fois, et retourner Z à la derniere fois 
*/