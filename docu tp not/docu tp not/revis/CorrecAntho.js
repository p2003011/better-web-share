const ex4 =
  (func, def) =>
  (...args) =>
    ((ret) => ret ?? def)(func(...args));

// ex4 est une fonction qui prend en paramètres :
// - Une fonction;
// - Une valeur par défaut (voir ci-dessous).
// On passe des arguments en paramètres de cette seconde fonction :
// - Si la fonction en param. renvoie ni null ni undefined, on renvoie
//   ce retour même;
// - Le cas échéant, ex4 renvoie la valeur par défaut à la place.
//
// Exemple :
//
// > | function maFonction(valeurEnParametre)
//   | { inutile = 1; // ça renvoie rien
//   | }
//   | ex4(maFonction, 101)(5)
// < | 101
//
// > | let appelsAMaFonction = 0
//   | function maFonction(valeurEnParametre)
//   | { appelsAMaFonction++; // ça renvoie rien
//   |   console.log(appelsAMaFonction); }
//   | ex4(maFonction, 101)(5)
// ? | 1
// < | 101
//
// La ligne avec le ? correspond à l'unique log effectué par maFonction.
// Entre autres, elle renvoie le nombre total d'appels à elle : 1.